package se.plankter.genalg;

/**
 * 
 * @author Bjorn P Mattsson
 *
 */
public interface IChromosomeDecoder {

	/**
	 * Decodes the chromosome to return a fitness value. This is problem
	 * specific and a new decoder will need to be implemented for every new
	 * problem. A good solution/chromosome should return a higher fitness value.
	 * 
	 * @param chromosome
	 *            The chromosome to decode
	 * @return The fitness value.
	 */
	double decodeChromosome(IChromosome chromosome);
}
