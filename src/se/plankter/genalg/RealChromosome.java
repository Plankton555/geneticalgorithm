package se.plankter.genalg;

import java.util.Random;

/**
 * A chromosome containing real-valued numbers between 0 and 1.
 * 
 * @author Bjorn P Mattsson
 * 
 */
public class RealChromosome implements IChromosome {

	/**
	 * Generates a uniformly random chromosome of the specified length.
	 * 
	 * @param chromosomeLength
	 *            The length of the chromosome
	 * @return Randomly generated chromosome
	 */
	public static RealChromosome getRandomChromosome(int chromosomeLength) {
		return new RealChromosome(chromosomeLength);
	}

	private double[] chromosome;
	private double creepValue = 0.1;

	/**
	 * Creates a new random chromosome of the specified length. The genes will
	 * have values between 0 and 1.
	 * 
	 * @param chromosomeLength
	 *            The length of the chromosome
	 */
	private RealChromosome(int chromosomeLength) {
		this.chromosome = new double[chromosomeLength];

		for (int i = 0; i < chromosomeLength; i++) {
			chromosome[i] = Math.random();
		}
	}

	/**
	 * Creates a new chromosome with the same genome as that of the original
	 * chromosome.
	 * 
	 * @param origin
	 *            Original chromosome
	 */
	private RealChromosome(RealChromosome origin) {
		this.chromosome = new double[origin.chromosome.length];

		for (int i = 0; i < origin.chromosome.length; i++) {
			chromosome[i] = origin.chromosome[i];
		}
	}

	/**
	 * Performs a 1-point (random) crossover on the two chromosomes.
	 * 
	 * @return Array containing two chromosomes
	 */
	@Override
	public IChromosome[] crossover(IChromosome c1, IChromosome c2) {
		if (c1.getChromosomeLength() != c2.getChromosomeLength()) {
			throw new IllegalArgumentException("Different chromosome lengths");
		}
		if (!(c1 instanceof RealChromosome) || !(c2 instanceof RealChromosome)) {
			throw new IllegalStateException(
					"RealChromosome cannot perform crossover on something"
							+ " that is not a RealChromosome");
		}
		RealChromosome rc1 = (RealChromosome) c1;
		RealChromosome rc2 = (RealChromosome) c2;
		RealChromosome[] output = { (RealChromosome) rc1.getCopy(),
				(RealChromosome) rc2.getCopy() };
		int chromoLength = c1.getChromosomeLength();
		int crossoverPoint = (int) (Math.random() * chromoLength);
		for (int i = crossoverPoint; i < chromoLength; i++) {
			output[0].chromosome[i] = rc2.chromosome[i];
			output[1].chromosome[i] = rc1.chromosome[i];
		}
		return output;
	}

	@Override
	public int getChromosomeLength() {
		return chromosome.length;
	}

	/**
	 * @return A new chromosome with the same gene content
	 */
	@Override
	public IChromosome getCopy() {
		return new RealChromosome(this);
	}

	/**
	 * Returns the gene at position i.
	 * 
	 * @param i
	 *            Gene position
	 * @return Gene at position i
	 */
	public double getGene(int i) {
		return chromosome[i];
	}

	@Override
	public void mutate(double mutationProbability) {
		for (int i = 0; i < chromosome.length; i++) {
			if (Math.random() < mutationProbability) {
				// Do mutation
				Random r = new Random();
				double mut = r.nextGaussian() * creepValue;
				double newVal = chromosome[i] + mut;
				if (newVal < 0) {
					newVal = 0;
				} else if (newVal > 1) {
					newVal = 1;
				}
				chromosome[i] = newVal;
			}
		}
	}

	@Override
	public String toString() {
		StringBuilder output = new StringBuilder("Chromosome{");
		for (int i = 0; i < chromosome.length; i++) {
			output.append(chromosome[i]);
			if (i < chromosome.length - 1) {
				output.append(",");
			}
		}
		output.append("}");

		return output.toString();
	}

	/**
	 * Sets the creep value. A larger creep value results in larger mutations.
	 * The creep value must be non-negative. The default value is 0.1.
	 * 
	 * @param creepValue
	 *            The creep value
	 */
	public void setCreepValue(double creepValue) {
		if (creepValue < 0) {
			throw new IllegalArgumentException(
					"Creep value must be non-negative");
		}
		this.creepValue = creepValue;
	}

	/**
	 * @return The creep value
	 */
	public double getCreepValue() {
		return this.creepValue;
	}

}
