package se.plankter.genalg;

/**
 * 
 * @author Bjorn P Mattsson
 * 
 */
public class TournamentSelection implements ISelection {

	private double pTour;

	/**
	 * Creates a new TournamentSelection with the specified tournament selection
	 * parameter. Tournament selection picks two random individuals and compares
	 * their fitness values. The selection parameter determines the probability
	 * that the best of the two selected individuals is returned.
	 * The tournament selection parameter should not be lower than 0.5 (since 
	 * that would favour negative performance). 0.6-0.9 is a common interval for
	 * the parameter.
	 * 
	 * @param tournamentSelectionParameter
	 *            Probability that the better individual is selected
	 */
	public TournamentSelection(double tournamentSelectionParameter) {
		this.pTour = tournamentSelectionParameter;
	}

	@Override
	public int doSelection(double[] fitnessValues) {
		int i1 = (int) (Math.random() * fitnessValues.length);
		int i2 = (int) (Math.random() * fitnessValues.length);

		int iBest = -1;
		int iWorst = -1;
		if (fitnessValues[i1] > fitnessValues[i2]) {
			iBest = i1;
			iWorst = i2;
		} else {
			iBest = i2;
			iWorst = i1;
		}

		if (Math.random() < pTour) {
			return iBest;
		} else {
			return iWorst;
		}
	}
}
