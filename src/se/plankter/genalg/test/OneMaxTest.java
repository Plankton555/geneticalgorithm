package se.plankter.genalg.test;

import se.plankter.genalg.BinaryChromosome;
import se.plankter.genalg.GeneticAlgorithm;
import se.plankter.genalg.IChromosomeDecoder;
import se.plankter.genalg.ISelection;
import se.plankter.genalg.TournamentSelection;

public class OneMaxTest {

	private static double P_TOUR = 0.75;

	public static void main(String[] args) {
		int populationSize = 20;
		int chromosomeLength = 100;
		BinaryChromosome[] population = new BinaryChromosome[populationSize];
		for (int i = 0; i < populationSize; i++) {
			population[i] = BinaryChromosome
					.getRandomChromosome(chromosomeLength);
		}

		ISelection selection = new TournamentSelection(P_TOUR);
		IChromosomeDecoder decoder = new OnemaxDecoder();

		GeneticAlgorithm ga = new GeneticAlgorithm(population, selection,
				decoder);

		for (int i = 0; i < 100; i++) {
			ga.runGeneration();
			System.out.println("G: " + (i + 1) + "\tfitness: "
					+ ga.getBestFitness());
			// System.out.println(ga.getBestChromosome().toString());
		}

		System.out.println("\nTest of Genetic Algorithm has been run");
	}
}
