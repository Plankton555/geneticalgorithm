package se.plankter.genalg.test;

import se.plankter.genalg.BinaryChromosome;
import se.plankter.genalg.IChromosome;
import se.plankter.genalg.IChromosomeDecoder;

/**
 * 
 * @author Bjorn P Mattsson
 *
 */
public class OnemaxDecoder implements IChromosomeDecoder {

	@Override
	public double decodeChromosome(IChromosome chromosome) {
		// Onemax
		if (!(chromosome instanceof BinaryChromosome)) {
			throw new IllegalStateException(
					"OnemaxDecoder cannot decode something that is not "
							+ "a BinaryChromosome");
		}
		BinaryChromosome bc = (BinaryChromosome) chromosome;
		double fitness = 0;

		for (int i = 0; i < bc.getChromosomeLength(); i++) {
			if (bc.getGene(i) == 1) {
				fitness++;
			}
		}
		return fitness;
	}
}
