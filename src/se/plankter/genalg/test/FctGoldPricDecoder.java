package se.plankter.genalg.test;

import se.plankter.genalg.IChromosome;
import se.plankter.genalg.IChromosomeDecoder;
import se.plankter.genalg.RealChromosome;

public class FctGoldPricDecoder implements IChromosomeDecoder {

	@Override
	public double decodeChromosome(IChromosome chromosome) {
		// Goldstein�Price function
		if (!(chromosome instanceof RealChromosome)) {
			throw new IllegalStateException(
					"FunctionDecoder cannot decode something that is not "
							+ "a RealChromosome");
		}
		RealChromosome rc = (RealChromosome) chromosome;

		double minValue = -2;
		double maxValue = 2;

		// First variable
		double x = minValue + (maxValue - minValue) * rc.getGene(0);
		// Second variable
		double y = minValue + (maxValue - minValue) * rc.getGene(1);

		// Goldstein�Price function
		double fVal = (1 + Math.pow(x + y + 1, 2)
				* (19 - 14 * x + 3 * x * x - 14 * y + 6 * x * y + 3 * y * y))
				* (30 + Math.pow(2 * x - 3 * y, 2)
						* (18 - 32 * x + 12 * x * x + 48 * y - 36 * x * y + 27
								* y * y));
		double fitness = 1 / fVal; // finding the minimum

		return fitness;
	}
}
